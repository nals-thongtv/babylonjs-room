import '@babylonjs/loaders/glTF';
// import '@babylonjs/loaders/OBJ';

import './App.css';
import {
  Engine, Scene, Vector3, HemisphericLight, SceneLoader, UniversalCamera, MeshBuilder, PointerEventTypes, ActionManager, ExecuteCodeAction,
  Animation,
  StandardMaterial,
  Texture,
  Color3,
  TransformNode,
} from '@babylonjs/core';
import {
  useCallback, useEffect, useRef,
} from 'react';
import * as GUI from '@babylonjs/gui';



// Register hover
// const registerHoverOnMesh = ({mesh, scene, advancedTexture, text }) => {
//   let rect1 = new GUI.Rectangle();
//   advancedTexture.addControl(rect1);
//   rect1.width = "150px";
//   rect1.height ="30px";
//   rect1.thickness = 2;  
//   rect1.linkOffsetY = "-100px";
//   rect1.transformCenterY = 1;  
//   rect1.background = "white";
//   rect1.alpha = 0.5;
//   rect1.scaleX = 0;
//   rect1.scaleY = 0;
//   rect1.cornerRadius = 5
//   rect1.linkWithMesh(mesh);    
  
//   let text1 = new GUI.TextBlock();
//   text1.text = text;
//   text1.color = "red";
//   text1.fontSize = 18;
//   text1.textWrapping = true;
//   text1.textVerticalAlignment = GUI.Control.VERTICAL_ALIGNMENT_TOP;
//   text1.background = '#006994'
//   rect1.addControl(text1)
//   text1.alpha = (1/text1.parent.alpha);

//   const actionManager = new ActionManager(scene);
//   mesh.actionManager = actionManager

//   let scaleXAnimation = new Animation("myAnimation", "scaleX", 30, Animation.ANIMATIONTYPE_FLOAT, Animation.ANIMATIONLOOPMODE_CONSTANT);
//   let scaleYAnimation = new Animation("myAnimation", "scaleY", 30, Animation.ANIMATIONTYPE_FLOAT, Animation.ANIMATIONLOOPMODE_CONSTANT);

//   var keys = [];

//   keys.push({
//     frame: 0,
//     value: 0
//   });
//   keys.push({
//     frame: 10,
//     value: 1
//   });

//   scaleXAnimation.setKeys(keys);
//   scaleYAnimation.setKeys(keys);
//   rect1.animations = [];
//   rect1.animations.push(scaleXAnimation);
//   rect1.animations.push(scaleYAnimation);

//   actionManager.registerAction(new ExecuteCodeAction(ActionManager.OnPointerOverTrigger, function(ev){
//     scene.beginAnimation(rect1, 0, 10, false);
//   }));
//   //if hover is over remove highlight of the mesh
//   actionManager.registerAction(new ExecuteCodeAction(ActionManager.OnPointerOutTrigger, function(ev){
//     scene.beginAnimation(rect1, 10, 0, false);
//   }));

// } 

function App() {
  const canvasElRef = useRef();
  const sceneRef = useRef()

  const createArtwork = ({ name, width, height, depth, artworkUrl }) => {
    const group = new TransformNode()

    const frame = MeshBuilder.CreateBox(`frame-${name}`, {height, width, depth});
    const artwork = MeshBuilder.CreatePlane(`artwork-${name}`, {height, width});
    artwork.translate(new Vector3(0, 0, -depth/2), 1)

    const image = new StandardMaterial("decalMat", sceneRef.current);
    image.diffuseTexture = new Texture(artworkUrl, sceneRef.current);
    artwork.material = image

    frame.parent = group
    artwork.parent = group
    return group
  }

  const onSceneReady = useCallback((scene) => {
    const camera = new UniversalCamera('UniversalCamera', new Vector3(0, 2, -3), scene);
    camera.applyGravity = true;
    camera.checkCollisions = true
    camera.speed = 0.5

    // This targets the camera to the first artwork
    camera.setTarget(new Vector3(0, 2, 0));

    const canvas = scene.getEngine().getRenderingCanvas();

    // This attaches the camera to the canvas
    camera.attachControl(canvas, true);

    const light = new HemisphericLight('light', new Vector3(0, 2, -1), scene);
    light.diffuse = new Color3(2, 2, 2)

    // Default intensity is 1. Let's dim the light a small amount
    light.intensity = 0.7;

    // Picture 1
    const artwork1 = createArtwork({name: "box1", height: 1, width: 1.5, depth: 0.25, artworkUrl: "/artworks/cat.jpeg"})
    artwork1.position = new Vector3(0, 2, 5.75)

    // Picture 2
    const artwork2 = createArtwork({name: "box1", height: 1, width: 1.5, depth: 0.25, artworkUrl: "/artworks/dog.jpeg"})
    artwork2.position = new Vector3(5.75, 2, 0)
    artwork2.rotation.y = Math.PI / 2 

    // Trigger update camera position when click to Picture
    scene.onPointerObservable.add((pointerInfo) => {
      switch (pointerInfo.type) {
      case PointerEventTypes.POINTERDOWN:
        if (pointerInfo.pickInfo.hit && pointerInfo.pickInfo.pickedMesh) {
          scene.onPointerPick = function (evt, pickInfo){
            window.pickedMesh = pickInfo.pickedMesh
            // TODO: calculate position base on pickedMesh
            camera.position = new Vector3(0, 2, 0)
            camera.target = pickInfo.pickedMesh.absolutePosition
          }
        }
        break;
      default:
        break;
      }
    });

    // // GUI
    // const advancedTexture = GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI");
    // advancedTexture.useInvalidateRectOptimization = false;

    // registerHoverOnMesh({ scene, mesh: box1, advancedTexture, text: 'Picture 1'})
    // registerHoverOnMesh({ scene, mesh: box2, advancedTexture, text: 'Picture 2'})

  }, []);

  useEffect(() => {
    const { current: canvas } = canvasElRef;

    if (!canvas) return;

    const engine = new Engine(canvas);
    const scene = new Scene(engine);
    scene.collisionsEnabled = true
    scene.gravity = new Vector3(0, -0.15, 0);

    sceneRef.current = scene

    if (scene.isReady()) {
      onSceneReady(scene);
    } else {
      scene.onReadyObservable.addOnce((scene) => onSceneReady(scene));
    }

    // Import Room 
    SceneLoader.ImportMesh(null, '/models/', 'room04.glb', scene, (meshes) => {
      meshes.forEach((mesh) => {
        mesh.checkCollisions = true
        mesh.isPickable = false
      });
    });  

    engine.runRenderLoop(() => {
      scene.render();
    });

    const resize = () => {
      scene.getEngine().resize();
    };

    if (window) {
      window.addEventListener('resize', resize);
    }

    return () => {
      scene.getEngine().dispose();

      if (window) {
        window.removeEventListener('resize', resize);
      }
    };
  }, [onSceneReady]);

  return <div>
    <canvas ref={canvasElRef} style={{ width: '100vw', height: '100vh' }} />
  </div>;
}

export default App;
