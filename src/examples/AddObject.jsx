import '@babylonjs/loaders/glTF';

import './App.css';
import {
  Engine, Scene, Vector3, HemisphericLight, SceneLoader, PointerEventTypes, UtilityLayerRenderer, PositionGizmo, RotationGizmo, UniversalCamera,
} from '@babylonjs/core';
import {
  useCallback, useEffect, useRef,
} from 'react';

// console.log(mesh)

function App() {
  let xAxis;
  let yAxis;
  let zAxis;
  // let
  // let box
  const canvasElRef = useRef();
  const sceneRef = useRef()

  const onSceneReady = useCallback((scene) => {
    // This creates and positions a free camera (non-mesh)
    const camera = new UniversalCamera('UniversalCamera', new Vector3(0, 0, -10), scene);
    camera.applyGravity = true;
    camera.checkCollisions = true
    camera.speed = 0.5
    // camera.maxZ = 0;

    // scene.collisionsEnabled = true
    // This targets the camera to scene origin
    camera.setTarget(Vector3.Zero());

    const canvas = scene.getEngine().getRenderingCanvas();

    // This attaches the camera to the canvas
    camera.attachControl(canvas, true);

    // This creates a light, aiming 0,1,0 - to the sky (non-mesh)
    const light = new HemisphericLight('light', new Vector3(0, 1, 0), scene);

    // Default intensity is 1. Let's dim the light a small amount
    light.intensity = 0.7;

    // Our built-in 'box' shape.
    // box = MeshBuilder.CreateBox("box", { size: 2 }, scene);

    // // Move the box upward 1/2 its height
    // box.position.y = 1;

    // Our built-in 'ground' shape.
    // MeshBuilder.CreateGround("ground", { width: 6, height: 6 }, scene);
  }, []);

  /**
 * Will run on every frame render.  We are spinning the box on y-axis.
 */
  // const onRender = (scene) => {
  //   if (box !== undefined) {
  //     var deltaTimeInMillis = scene.getEngine().getDeltaTime();

  //     const rpm = 10;
  //     box.rotation.y += (rpm / 60) * Math.PI * 2 * (deltaTimeInMillis / 1000);
  //   }
  // };

  useEffect(() => {
    const { current: canvas } = canvasElRef;
    // let roomMeshes

    if (!canvas) return;

    const engine = new Engine(canvas);
    const scene = new Scene(engine);
    scene.collisionsEnabled = true
    scene.gravity = new Vector3(0, -0.15, 0);

    sceneRef.current = scene

    if (scene.isReady()) {
      onSceneReady(scene);
    } else {
      scene.onReadyObservable.addOnce((scene) => onSceneReady(scene));
    }

    SceneLoader.ImportMesh(null, '/models/', 'collision-world.glb', scene, (meshes) => {
      // roomMeshes = meshes
      meshes.forEach((mesh) => {
        mesh.isPickable = false;
        mesh.checkCollisions = true
      });
      // scene.createDefaultCameraOrLight(true, true, true);
      // scene.createDefaultEnvironment();
      // scene.activeCamera.alpha = Math.PI / 2;
    });

    

    SceneLoader.LoadAssetContainer('https://models.babylonjs.com/', 'seagulf.glb', scene, (container) => {
      container.addAllToScene();
      container.meshes[0].scaling.scaleInPlace(0.002);
      // container.meshes[0].position.x = -1
    });

    engine.runRenderLoop(() => {
      // if (typeof onRender === "function") onRender(scene);
      scene.render();
    });

    const resize = () => {
      scene.getEngine().resize();
    };

    if (window) {
      window.addEventListener('resize', resize);
    }

    scene.onPointerObservable.add((pointerInfo) => {
      switch (pointerInfo.type) {
      case PointerEventTypes.POINTERDOWN:
        // console.log(pointerInfo.pickInfo.hit)
        if (pointerInfo.pickInfo.hit && pointerInfo.pickInfo.pickedMesh) {
          // console.log(pointerInfo.pickInfo.pickedMesh)
          // const { pickedMesh } = pointerInfo.pickInfo;

          // pickedMesh.showBoundingBox = true;

          const utilLayer = new UtilityLayerRenderer(scene);
          const positionGizmo = new PositionGizmo(utilLayer, 3);
          const rotationGizmo = new RotationGizmo(utilLayer);
          

          scene.onPointerPick = function (evt, pickInfo){
            console.log(pickInfo)
            positionGizmo.attachedMesh = pickInfo.pickedMesh;
            positionGizmo.xGizmo = 3

            rotationGizmo.attachedMesh = pickInfo.pickedMesh
          }

          // var greenMat = new StandardMaterial("ground", scene);
          // greenMat.diffuseColor = new Color3(0.4, 0.4, 0.4);
          // greenMat.specularColor = new Color3(0.4, 0.4, 0.4);
          // greenMat.emissiveColor = Color3.Green();

          // var blueMat = new StandardMaterial("ground", scene);
          // blueMat.diffuseColor = new Color3(0.4, 0.4, 0.4);
          // blueMat.specularColor = new Color3(0.4, 0.4, 0.4);
          // blueMat.emissiveColor = Color3.Blue();

          // var purpleMat = new StandardMaterial("ground", scene);
          // purpleMat.diffuseColor = new Color3(0.4, 0.4, 0.4);
          // purpleMat.specularColor = new Color3(0.4, 0.4, 0.4);
          // purpleMat.emissiveColor = Color3.Purple();

          // yAxis = MeshBuilder.CreateBox("box", { height: 1, width: 0.25, depth: 0.25 }, scene)
          // yAxis.material = greenMat
          // yAxis.position.y = 1

          // xAxis = MeshBuilder.CreateBox("box", { height: 0.25, width: 1, depth: 0.25 }, scene)
          // xAxis.material = blueMat
          // xAxis.position.x = 1
          // xAxis.position.y = 0.5

          // zAxis = MeshBuilder.CreateBox("box", { height: 0.25, width: 0.25, depth: 1 }, scene)
          // zAxis.material = purpleMat
          // // zAxis.position.x = 1
          // zAxis.position.z = -1
        }
        // console.log(roomMeshes.includes(pointerInfo.pickInfo.pickedMesh))
        // if(pointerInfo.pickInfo.hit && pointerInfo.pickInfo.pickedMesh != ground) {
        //             pointerDown(pointerInfo.pickInfo.pickedMesh)
        //         }
        break;
      case PointerEventTypes.POINTERUP:
        // pointerUp();
        break;
      case PointerEventTypes.POINTERMOVE:
        // pointerMove();
        break;
      default:
        break;
      }
    });

    // canvas.addEventListener("pointerdown", onPointerDown, false);

    return () => {
      scene.getEngine().dispose();

      if (window) {
        window.removeEventListener('resize', resize);
      }
    };
  }, [onSceneReady, xAxis, yAxis, zAxis]);

  const handleAddObject = () => {
    SceneLoader.LoadAssetContainer('https://models.babylonjs.com/', 'seagulf.glb', sceneRef.current, (container) => {
      container.addAllToScene();
      container.meshes[0].scaling.scaleInPlace(0.002);
      // container.meshes[0].position.x = -1
    });
  }

  return <div>
    <button className='button-add-object' onClick={handleAddObject}>Add object</button>
    <canvas ref={canvasElRef} style={{ width: '100vw', height: '100vh' }} />
  </div>;
}

export default App;
